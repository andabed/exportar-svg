#!/bin/zsh

# Este script se encuentra bajo la licencia CC BY 4.0
# http://creativecommons.org/licenses/by/4.0/
#   Autor: andabed
#   Repositorio personal: https://gitlab.com/andabed
#   Repositorio script: https://gitlab.com/andabed/exportar-svg
   
# Versión de script 2.0
echo "[====================== SCRIPT INICIADO ===================]"

# Guarda titulos de archivo en texto plano
ls | grep .svg > titulos

# Cuenta número de lineas y lo pasa a número
num=$( wc -l titulos )
num=$(echo $num | sed 's/titulos//g')
declare -i num

# Creando arreglo con los nombre dei los archivos
nombres_archivos=()

# Ciclo para filtrar todos los que son svg's
for (( i=1;i<=$num;i++ ))
do
    #Lee lineas del archivo titulos y elimina la extensión
    nombre=$( awk 'NR=='$i titulos | sed 's/.svg//g' )
    nombres_archivos+=($nombre)
done

# Imprime los svg's que han sido encontrados
echo "- svg's identificados: "

for (( i=1;i<=$num;i++ ))
do
    echo "      "$i" "$nombres_archivos[$i]".svg"
done

echo "TOTAL DE SVG'S: "$num
echo "[=========================================================]"

# Creando directorio para guardar pdf
mkdir pdf 2> /dev/null

# Realizando la conversión a pdf y moviendolos a pdf
for (( i=1;i<=$num;i++ ))
do
    inkscape --export-pdf=$nombres_archivos[$i].pdf $nombres_archivos[$i].svg 2> /dev/null
    mv $nombres_archivos[$i].pdf ./pdf
    echo $nombres_archivos[$i]" exportado"
    echo "[=========================================================]"
done

# Da la opción de borrar svgs
echo "               ====================="
echo "               | SVG exportados :) |"
echo "               ====================="
echo "[=========================================================]"
echo -n "¿Desea borrar los .svg? (s/n)? : "
read borrar_svg

#analiza respuesta dl usuario
if [[ $borrar_svg == s ]]
then
    rm -f *.svg
fi

#Borra el archivo temporal con lo titulos
rm titulos

echo "[===================== SCRIPT FINALIZADO ===================]"

