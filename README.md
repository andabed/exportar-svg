# EXPORTAR A SVG
[![forthebadge](img/bookmark.svg)](https://forthebadge.com)

[![andabed](https://img.shields.io/badge/andabed-gitlab-br?style=flat-square)](https://gitlab.com/andabed)
[![Licencia](https://img.shields.io/badge/Licencia-CC%20BY%204.0-red)](http://creativecommons.org/licenses/by/4.0/)

Script que permite exportar los `svg's` de un directorio a `png` y `pdf` de forma masiva.

# Tabla de contenido

- [Requisitos](#requisitos)
- [Recomendaciones](#recomendaciones)
- [Uso](#uso)
- [Licencia](#licencia)

# Requisitos

[(Regresar a la tabla de contenido)](#tabla-de-contenido)

1. Tener el interprete de comando oh-my-zsh. En caso de no tenerlo se puede instalar de la siguiente [forma](https://qirolab.com/posts/install-and-setup-oh-my-zsh-on-ubuntu-system).
2. No tener archivos o directorios con los siguientes nombres: `titulos, pdf, png`.

# Recomendaciones 

[(Regresar a la tabla de contenido)](#tabla-de-contenido)

1. El interprete oh-my-zsh debe estar corriendo en su terminal. En caso de tenerlo instalado y no estar ejecutandolo lo puede hacer con el siguiente comando
  ```
  zsh
  ```
2. Evitar el uso de caracteres especiales como `\ *`
3. **No** hay problema en tener otros archivos de cualquier extensión en el directorio donde se encuentran los `svg` dado que el script filtra solo los que poseen una extensión `.svg`.
4. Si desea exportar el `svg` tanto a `png` como a `pdf` puede ejecutar ambos scripts de forma seguida (Uno luego de otro).

# Uso

[(Regresar a la tabla de contenido)](#tabla-de-contenido)

1. Seleccionar el formato a exportar llos `svg`. Para exportar a `pdf` ejecutar el script `svg-to-pdf.sh`y para exportar a `png` ejecutar el script `svg-to-png.sh`
2. Mover el script seleccionado al directorio donde se encuentra todos los archivos `svg` a exportar.
3. Ejecutar script de la siguiente forma
  ```
  ./nombre-script.sh
  ```
4. Durante la ejecución del script se le va a preguntar si desea borrar todos los archivos `svg`, puede contestar ingresando la letra `s` haciendo referencia a que **Si los desea borrar** y la letra `n` haciendo referencia a que **No los desea borrar**.
5. En el directorio donde se encuentra el script se debió crear una carpeta llamada `png` o `pdf` dependiendo del script que ejecutó; dentro de la carpeta se encuentra exportados los archivos `svg`.

# Licencia

[(Regresar a la tabla de contenido)](#tabla-de-contenido)

Este trabajo está licenciado bajo
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://img.shields.io/badge/Licencia-CC%20BY%204.0-red
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
